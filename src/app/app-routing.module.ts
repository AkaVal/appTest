import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { NewUserComponent } from './Component/user/newUser/NewUser.component';
import { ListUserComponent } from './Component/user/listUser/ListUser.component';
import { ShowUserComponent } from './Component/user/show-user/show-user.component';


const routes: Routes = [
  { path : 'users/new', component: NewUserComponent},
  { path : 'users', component: ListUserComponent}, 
  { path : 'users/:hash', component: ShowUserComponent},
  { path : 'not-found', redirectTo: 'users', pathMatch: 'full'},
  { path: '', redirectTo: 'users', pathMatch: 'full'  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
