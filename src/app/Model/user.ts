export class User {
    id: number;
    firstname: string;
    lastname: string;
    phone: string;
    email: string;
    password: string;
    link: string;
    hash: string;

    constructor (id?:number, firstname?:string, lastname?:string, phone?:string, email?:string, password?:string, link?:string, hash?:string){
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.link = link;
        this.hash = btoa(this.link);
    }
}

