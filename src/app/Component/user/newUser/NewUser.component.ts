
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/Model/user';
import { environment } from 'src/environments/environment';
import { Platform } from '@ionic/angular';
import { HttpClient,HttpClientModule ,HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http'

@Component({
  selector: 'app-user',
  templateUrl: './NewUser.component.html',
  styleUrls: ['./NewUser.component.css']
})
export class NewUserComponent implements OnInit {
  APIUrl = '';
  user: User;
  users : User[];
  submitted = false;
  userForm: User;
  errors;
  pswUnvalid = false;
  pswUnvalid2 = false;
  pswUnvalid3 = false;
  pswUnvalid4 = false;
  firstnameMissing = false;
  lastnameMissing = false;
  emailMissing = false;
  pswMissing = false;
  phoneMissing = false;
  emailUnvalid = false
  constructor(private http:HttpClient, public platform: Platform) { }

  ngOnInit(): void {

    if(this.platform.is("android")){
        this.APIUrl = environment.AndroidUrl;
    }else{
        this.APIUrl = environment.LocalUrl;
    }
    this.APIUrl += "simple-api/users/new";
    this.user = new User();
    this.userForm = new User();
  }
  onSubmit(){
    let form = this.userForm;
    let datas = new User(null,form.firstname,form.lastname,form.phone,form.email,form.password,null);
    this.sendDataAPI(this.APIUrl,datas).subscribe(
      then => {
        alert("L'utilisateur à été bien ajouter");
        this.userForm = new User();
      },
      (error) => {
        this.submitted = true;
        this.errors = error.error['data'];
        this.formValidator(this.errors);
        return;
      },
    )
  }
  sendDataAPI(URL: string, data:User){

    return this.http.post(URL,
      data,
    {
      headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'SimpleApiKey': 'myOwnSecretApiKey'
      })
    },);
  }

    formValidator(datas: Array<string>){
        this.pswUnvalid = false;
        this.pswUnvalid2 = false;
        this.pswUnvalid3 = false;
        this.pswUnvalid4 = false;
        this.firstnameMissing = false;
        this.lastnameMissing = false;
        this.emailMissing = false;
        this.pswMissing = false;
        this.phoneMissing = false;
        this.emailUnvalid = false
        datas.forEach(data => {
            switch (data) {
                case "field 'password' is not a valid password: must contains at least 8 characters, a digit (0-9), an uppercase letter (A-Z)":
                    this.pswUnvalid = true
                    break;
                case "field 'password' is not a valid password: must contains a digit (0-9), an uppercase letter (A-Z)":
                    this.pswUnvalid2 = true
                    break;
                case "field 'password' is not a valid password: must contains an uppercase letter (A-Z)":
                    this.pswUnvalid3 = true
                    break;
                case "field 'password' is not a valid password: must contains a digit (0-9)":
                    this.pswUnvalid4 = true
                    break;
                case "missing required field 'firstname'":
                    this.firstnameMissing = true
                    break;
                case "missing required field 'lastname'":
                    this.lastnameMissing = true
                    break;
                case "missing required field 'email'":
                    this.emailMissing = true
                    break;
                case "missing required field 'password'":
                    this.pswMissing = true
                    break;
                case "missing required field 'phone'":
                    this.phoneMissing = true
                    break;
                case "field 'email' is not a valid email address":
                    this.emailUnvalid = true
                    break;
                default:
                    break;
            }
        });
    }
}


