import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/Model/user';
import { environment } from 'src/environments/environment';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-show-user',
  templateUrl: './show-user.component.html',
  styleUrls: ['./show-user.component.scss'],
})
export class ShowUserComponent implements OnInit {
    APIUrl = '';
    user: User;
    
    constructor(private http:HttpClient, private route: ActivatedRoute, private router: Router, public platform: Platform) { }
    
    ngOnInit(): void {
        if(this.platform.is("android")){
            this.APIUrl = environment.AndroidUrl;
        }else{
          this.APIUrl = environment.LocalUrl;
        }
        this.APIUrl = this.APIUrl.slice(0, -1);

        this.route.params.subscribe(params => {
            try {
                this.APIUrl += atob(params['hash']);
            } catch (err) {
                this.router.navigate(['/not-found']);
            }
        });

        this.readAPI(this.APIUrl).subscribe(
                (response) => {
                    var data = response['data'];
                    this.user = new User(null, data.firstname, data.lastname, null, data.email)
                },
                (error) => {
                    this.router.navigate(['/not-found']);
                }
        );
    }

    readAPI(URL: string){
        return this.http.get(URL,
            { headers: new HttpHeaders({'Content-Type': 'application/json','SimpleApiKey': 'myOwnSecretApiKey'})}
          );
    }

}
