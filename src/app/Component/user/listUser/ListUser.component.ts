
import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/Model/user';
import { HttpClient, HttpClientModule, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http'
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Platform } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';


@Component({
  selector: 'app-user',
  templateUrl: './ListUser.component.html',
  styleUrls: ['./ListUser.component.css']
})
export class ListUserComponent implements OnInit {

  @Input() Message: string;

  APIUrl = '';
  user: User;
  users : User[];
  submitted = false;
  userForm: User;
  errors;
  constructor(private http:HttpClient, private route: Router, public platform: Platform, private geolocation: Geolocation) {  }

  ngOnInit(): void {
    if(this.platform.is("android")){
        this.APIUrl = environment.AndroidUrl;
    }else{
      this.APIUrl = environment.LocalUrl;
    }

    this.geolocation.getCurrentPosition().then((resp) => {
        console.log('OK');
    }).catch((error) => {
        console.log('Error getting location', error);
    });

    this.APIUrl += "simple-api/users";
    this.readAPI(this.APIUrl)
        .subscribe((dataReceived) => {
        var data = dataReceived['data'];
        this.users = new Array;
        data.forEach(data => {
            console.log(this.Message);
            var thisUser = new User(null,data.firstname,data.lastname,null,data.email,null,data.links[0].href,data.hash);
            //console.log(thisUser.email, thisUser.hash, thisUser.link, atob(thisUser.hash));
            this.users.push(thisUser);
        });
      });
  }

  readAPI(URL: string){

    return this.http.get(URL,
      { headers: new HttpHeaders({'Content-Type': 'application/json','SimpleApiKey': 'myOwnSecretApiKey'})}
    );
  }
}
